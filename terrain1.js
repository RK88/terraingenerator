let c;
let ctx;

function init(id)
{
c = document.getElementById(id);
ctx = c.getContext("2d");
}


function gridCanvas(id, stepSize)
{
canvasWidth = ctx.canvas.width;
canvasHeight = ctx.canvas.height;


iterationsX = Math.floor(canvasWidth/stepSize);
iterationsY = Math.floor(canvasHeight/stepSize);


	ctx.strokeStyle= "#CCCCCC";

	//vertical lines
	for (i = 1; i < iterationsX+1; i++)
	{
	ctx.beginPath();
	ctx.moveTo(i*stepSize,0);
        ctx.lineTo(i*stepSize,canvasHeight);
	ctx.stroke();
	}

	//horizontal lines
	for (i = 1; i < iterationsY+1; i++)
	{
	ctx.beginPath();
	ctx.moveTo(0,i*stepSize);
        ctx.lineTo(canvasWidth,i*stepSize);
	ctx.stroke();
	}

}

function paintTile(context, x, y, tileSize, color)
{
	context.fillStyle = color;
	context.beginPath();
	context.fillRect(x*tileSize, y*tileSize, tileSize, tileSize);
}


function paintTileRect(context, x1, y1, widthTiles, heightTiles, tileSize, color)
{
	context.fillStyle = color;
	context.beginPath();
	context.fillRect(x1*tileSize, y1*tileSize, widthTiles * tileSize, heightTiles * tileSize);
}

function rgbFromHeight(height)
{
let result = "#";
let r = (Math.pow(height, 0.5)*25.5);
let g = Math.pow((Math.max(100-height,1)),0.5) *25.5;
let b = 125;


console.log(Math.floor(b).toString(16));


let rhex = Math.floor(r).toString(16);
let ghex = Math.floor(g).toString(16);
let bhex = Math.floor(b).toString(16);

return result + rhex + ghex+ bhex;
	
}




init("canvas1");
paintTileRect(ctx, 0,0,100,100,10,'#006600');
paintTileRect(ctx, 22, 11, 5, 7, 10,'#008800');
paintTile(ctx,13, 13, 10, '#FF0000'); 
gridCanvas("canvas1",10);
console.log("rgbtoHeight=" + rgbFromHeight(40));


for (i = 0; i <100; i ++)
{
let posX = Math.floor(Math.random()*100)
let posY = Math.floor(Math.random()*100)
let color = rgbFromHeight(Math.floor(Math.random()*99));
paintTile(ctx, posX, posY, 10, color);	
}





