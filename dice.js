

function randomDiceNumber(diceSides)
{
return Math.ceil(Math.random()*diceSides);
}



function dice3d(dices, modifierValue)
{
let sum = 0;

	for (i=0;i<dices;i++)
	{
	sum+=randomDiceNumber(6);
	}
	sum+=modifierValue;
	return sum;
}

let totalSum = 0;
let samples = 1000;



for (i=0; i<samples; i++)
{
totalSum+=randomDiceNumber(6);
}

let averageValue = totalSum/samples;

console.log(averageValue);

